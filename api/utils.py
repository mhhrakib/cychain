from connect import connect_bc, connect_ipfs
from datetime import datetime, timedelta
from config import date_format
import time

string = "cloud_one"
# string = ""
bc_client = connect_bc(string)
# print(bc_client.info())
# print(bc_client.getinfo());
ipfs_client = connect_ipfs("cloud_one")
# print(date_format)


addresses = bc_client.getaddresses()
print(addresses)


def comma_separated(li):
    st = str(li)
    st = st.replace("[", "")
    st = st.replace("]", "")
    st = st.replace("'", "")
    # st = "\"" + st + "\""
    st = st.replace(" ", "")
    return st


def get_node(type):
    li = []
    admin_addresses = bc_client.listpermissions(type)
    current_wallet_addresses = bc_client.getaddresses()
    for address in admin_addresses:
        if address["address"] in current_wallet_addresses:
            li.append(address["address"])
    return li


def get_permission_dict(addresses):
    print("get_perm",addresses)
    list_permissions = bc_client.listpermissions()
    dict_permission = {}
    for address in addresses:
        per = []
        for li in list_permissions:
            if li['address'] == address:
                per.append(li['type'])
        dict_permission[address] = per
    return dict_permission


def streams_list():
    return bc_client.liststreams('*', True)


def get_stream_names():
    streams = bc_client.liststreams()
    name_list = [stream['name'] for stream in streams if stream['subscribed'] and stream['items']]
    return name_list


def get_publishers_list(stream):
    pubs = bc_client.liststreampublishers(stream)
    pub_list = [pub['publisher'] for pub in pubs]
    # print('printing here', pub_list)
    return pub_list


def stream_action(stream, method):
    if method == '0':
        bc_client.unsubscribe(stream)
        # print("Unsubscribe")
    elif method == '1':
        bc_client.subscribe(stream)
    else:
        pass


def get_items_by_stream(stream_name):
    max_data_byte = 60
    max_items = 2000
    bc_client.subscribe(stream_name)
    item_list = bc_client.liststreamitems(stream_name, False, max_items)
    print(item_list[0])
    li = []

    for item in item_list:
        dic = {}
        dic['txid'] = item['txid']
        dic['publisher'] = item['publishers'][0]
        dic['key'] = item['keys'][0]
        dic['data'] = item['data']
        # returns first 50 byte of data
        # hex_data = bc_client.gettxoutdata(item['txid'], 0, max_data_byte)
        # data = binascii.unhexlify(hex_data).decode()
        # dic['data'] = data
        li.append(dic)
    return li  # returns a list of dict


def stream_detail(stream):
    return get_items_by_stream(stream)


def generate_keys(start, end):
    dates = [start]
    start_obj = datetime.strptime(start, date_format)
    end_obj = datetime.strptime(end, date_format)
    one_day = timedelta(days=1)
    start_obj += one_day

    while start_obj <= end_obj:
        print(start_obj)
        dates.append(start_obj.strftime(date_format))
        start_obj += one_day

    return dates


def get_logs(stream, publisher, start, end=None):
    st = time.time()
    query = {
        "publisher": publisher
    }
    keys = []
    if end == None or start == end:
        keys.append(start)
    else:
        keys += generate_keys(start, end)
    # cid_sets = set()
    data_dict = {}
    # print(keys)
    for key in keys:
        query["key"] = key
        response = bc_client.liststreamqueryitems(stream, query, True)
        # print(response)
        for item in response:
            metadata = item["data"]["json"]
            # cid_sets.add(metadata["id"])
            cid = metadata["id"]
            if cid not in data_dict.keys():
                data_dict[cid] = {
                    "start" : metadata["start"],
                    "end" : metadata["end"],
                    "size" : metadata["size"],
                    "encrypted": metadata["encrypted"]
                }

    # data = ""
    # cids = list(cid_sets)
    # print(cids)
    # for cid in cids:
    #     res = ipfs_client.cat(cid)
    #     data += res.decode()
    # time.time()
    print("time is being printed here:------>", time.time() - st)
    return data_dict


def get_ipfs_data(cid):
    data = ipfs_client.cat(cid).decode()
    # print(data)
    return data.splitlines()


from flask import Blueprint, render_template, request, url_for, flash, redirect
from flask.json import jsonify
from connect import connect_bc
from api.utils import get_permission_dict
from api.utils import get_node
from api.utils import comma_separated
import mcrpc

bc_client = connect_bc("cloud_one")

bc_bp = Blueprint('bc', __name__, url_prefix='/blockchain')


@bc_bp.route('/', methods=['GET'])
def home():
    addrs = bc_client.getaddresses()
    # print("Printing here", addrs)
    print(bc_client.getinfo())
    return render_template('blockchain.html', node_info=bc_client.getinfo().as_dict(), peers=bc_client.getpeerinfo(), addresses=addrs,
                           permissions=get_permission_dict(addrs), segment='home')




@bc_bp.route('/blocks', methods=['GET'])
def get_blocks():
    num_blocks = bc_client.getblockcount()
    block_list = bc_client.listblocks([i for i in range(num_blocks - 99, num_blocks + 1)])
    # print(block_list)
    return render_template('block-list.html', blocks=block_list, num_blocks=num_blocks, segment='bc-blocks')


@bc_bp.route('/blocks/<block>', methods=['GET'])
def get_block(block):
    block_details = bc_client.getblock(block, verbose=True)
    print(block_details)
    tx_list = block_details.pop('tx', None)
    # print(block_details['tx'])
    return render_template('block.html', block=block, tx_list=tx_list, info=block_details, segment='block')


@bc_bp.route('/permissions', methods=['GET', 'POST'])
def manage_permissions():
    if request.method == 'GET':
        return render_template("permissions.html", addresses=bc_client.getaddresses(), adminaddresses=get_node("admin"),
                               permissions=get_permission_dict(bc_client.getaddresses()), segment='permission')

    elif request.method == "POST":
        admin = request.form['admin']
        to_addr = request.form['toaddress']
        permission_list = request.form.getlist('permissions')
        operation = request.form.getlist('grant')
    permission_list = comma_separated(permission_list)
    try:
        if operation[0] == "grant":
            bc_client.grantfrom(admin, to_addr, permission_list)
            flash(permission_list + " permissions granted from " + admin + " to " + to_addr, category='success')
            return render_template("permissions.html", addresses=bc_client.getaddresses(), adminaddresses=get_node("admin"),
                                   permissions=get_permission_dict(bc_client.getaddresses()), segment='index')

        elif operation[0] == "revoke":
            bc_client.revokefrom(admin, to_addr, permission_list)
            flash(permission_list + " permissions revoked from " + admin + " to " + to_addr, category='success')
            return render_template("permissions.html", addresses=bc_client.getaddresses(), adminaddresses=get_node("admin"),
                                   permissions=get_permission_dict(bc_client.getaddresses()), segment='index')
    except mcrpc.exceptions.RpcError:
        flash("Oops! An error occurred! Action couldn't not be perfomed", category='error')
    return redirect(url_for('.manage_permissions'))

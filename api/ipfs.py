from flask import Blueprint, render_template, jsonify
from connect import connect_ipfs
from api.utils import get_ipfs_data

ipfs_client = connect_ipfs("cloud_one")

ipfs_bp = Blueprint('ipfs', __name__, url_prefix='/ipfs')


def get_ipfs_object_details(pins_list):
    res = []
    for pin, _ in pins_list.items():
        tmp = ipfs_client.object.stat(pin).as_json()
        res.append(tmp)
    return res


@ipfs_bp.route('/', methods=['GET'])
def ipfs():
    id = ipfs_client.id().as_json()
    config = ipfs_client.config.get().as_json()
    peers = ipfs_client.bootstrap.list().as_json()
    pins = ipfs_client.pin.ls(type="recursive").as_json()
    total = len(pins['Keys'])
    return render_template("ipfs-home.html", id=id, config=config['Addresses'], peers=peers['Peers'], total=total, segment='ipfs')


@ipfs_bp.route('/pins', methods=['GET'])
def get_pins():
    pins = ipfs_client.pin.ls(type="recursive").as_json()
    total = len(pins['Keys'])
    return render_template("ipfs-pins.html", pins=pins['Keys'], total=total, segment='ipfs')

@ipfs_bp.route('/<cid>', methods=['GET'])
def get_data(cid):
    data = get_ipfs_data(cid)
    return jsonify(data)

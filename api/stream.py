from flask import Flask, Blueprint, request, render_template, redirect, url_for, \
    flash, jsonify
from api.utils import streams_list, get_node, stream_action, stream_detail, \
    get_stream_names, get_publishers_list
import mcrpc
from connect import connect_bc


stream_bp = Blueprint('stream', __name__, url_prefix='/stream')
c = connect_bc("cloud_one")


@stream_bp.route('/', methods=['GET'])
def stream():
    streams = streams_list()
    return render_template('stream.html', streams=streams, segment='view-stream')


@stream_bp.route('/action', methods=['GET'])
def action():
    # print(request.get_data())
    method = request.args.get('method', default = '2', type = str)
    stream = request.args.get('stream', default = '*', type = str)
    print(method, stream)
    try:
        if method == '1' or method == '0':
            stream_action(stream, method)
            flash('Action was successfully performed', category='success')
    except mcrpc.exceptions.RpcError:
        flash("Oops! An error occurred! Action couldn't not be perfomed", category='error')
    return redirect(url_for('.stream'))


@stream_bp.route('/create', methods = ['GET', 'POST'])
def create_stream():
    if request.method == "GET":
        return render_template('create-stream.html', addresses=get_node("create"), segment='create-stream')
    if request.method == "POST":
        from_addr = request.form['fromaddress']
        str_name = request.form['streamname']
        print(from_addr, str_name)
        try:
            c.createfrom(from_addr, "stream", str_name, True)
            flash("Stream " + str_name + " is created by : " + from_addr, category='success')
        except mcrpc.exceptions.RpcError:
            flash("Oops! An error occurred! Stream couldn't be created!", category='error')
        return redirect(url_for('.stream'))


@stream_bp.route('/<stream_id>', methods=['GET'])
def get_detail(stream_id):
    print(stream_id)
    try:
        items = stream_detail(stream_id)
        return render_template('stream_details.html', items=items, stream=stream_id, segment='index')
    except mcrpc.exceptions.RpcError:
        return "<h3>An error occurred while processing the request, perhaps the stream doesn't exist! <h3>", 404


@stream_bp.route('/subscribed', methods=['GET'])
def get_subscribed():
    return jsonify(get_stream_names())


@stream_bp.route('/<stream_id>/publishers', methods=['GET'])
def get_publishers(stream_id):
    return jsonify(get_publishers_list(stream_id))





from flask import Blueprint, render_template, request, url_for
from werkzeug.utils import redirect
from api.utils import get_logs, get_ipfs_data
from publish.logger import Parser
retrieve_bp = Blueprint('retrieve', __name__, url_prefix='/retrieve')


@retrieve_bp.route('/', methods=['GET', 'POST'])
def retrieve():
    if request.method == 'GET':
        return render_template("retrieve-form.html", segment = 'retrieve-by-date')
    if request.method == 'POST':
        stream_id = request.form['stream']
        publisher = request.form['publisher']
        # str_chk = request.form['all_streams']
        # pub_chk = request.form['all_pubs']
        # print(str_chk, pub_chk)
        daterange = request.form['daterange']
        start, end = daterange.split(" - ")
        res = stream_id + " " + publisher + " " + daterange
        print("Look here", res)
        # cids = get_logs(stream_id, publisher, start, end)
        data = get_logs(stream_id, publisher, start, end)
        # print("data is being printed")
        return render_template('retrieve-output.html', data=data, start=start,
        end=end, stream=stream_id, segment='retrieve-by-date')


@retrieve_bp.route('/<cid>', methods=['GET'])
def get_data(cid):
    data = get_ipfs_data(cid)
    parser = Parser()
    lines = [parser.parse(line) for line in data]
    return render_template('retrieve-file.html', data=data, lines=lines, segment='retrieve-by-cid')

@retrieve_bp.route('/by-cid', methods=['GET','POST'])
def get_by_cid():
    if request.method == 'GET':
        return render_template('retrieve-by-cid.html', segment='retrieve-by-cid')
    if request.method == 'POST':
        ipfs_cid = request.form['cid']
        return redirect(url_for("retrieve.get_data", cid=ipfs_cid))


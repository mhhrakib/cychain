# # with open("../test.txt", "r") as file:
# #     logs = file.read();

# # print(len(logs))

# from datetime import datetime, timedelta
# import random


# def generate_periods(start, end, durations):
#     durations = [timedelta(minutes=m) for m in durations]

#     total_duration = sum(durations, timedelta())
#     print(total_duration)
#     nb_periods = len(durations)
#     open_duration = (end - start) - total_duration

#     delays = sorted(timedelta(seconds=s) 
#                     for s in random.sample(range(0, int(open_duration.total_seconds())), nb_periods))
#     print(delays)
#     periods = []
#     periods_before = timedelta()
#     for delay, duration in zip(delays, durations):
#         # periods.append((start + delay + periods_before,
#         #                 start + delay + periods_before + duration))
#         periods.append(start + delay + periods_before + duration)
#         periods_before += duration

#     return periods

# durations = [32, 24, 4, 20, 40, 8, 27, 18, 3, 4, 500] 
# start_time = datetime(2019, 5, 2, 9, 0, 0)
# end_time = datetime(2019, 12, 2, 17, 0, 0)
# per = generate_periods(start_time, end_time, durations)
# dates = [tarikh.strftime("%d %B, %Y, %H:%M:%S") for tarikh in per]
# print(len(dates), dates)
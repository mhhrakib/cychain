import mcrpc
import ipfshttpclient
from config import RPC_CONFIG, RPC_CONFIG_other, RPC_CONFIG_local, IPFS_CONFIG,\
    IPFS_CONFIG_other


def connect_bc(to=""):
    RPC = None
    if to == "cloud_one":
        RPC = RPC_CONFIG
    elif to == "cloud_two":
        RPC = RPC_CONFIG_other
    else:
        RPC = RPC_CONFIG_local

    return mcrpc.RpcClient(RPC["HOST"], RPC["PORT"], RPC["USER"], RPC["PASS"], use_ssl=False)


def connect_ipfs(to=""):
    RPC = None
    if to == "cloud_one":
        RPC = IPFS_CONFIG
    elif to == "cloud_two":
        RPC = IPFS_CONFIG_other
    else:
        return ipfshttpclient.connect()
    return ipfshttpclient.connect(RPC)


if __name__ == '__main__':
    print()
    # pass
    # print(f"bc_client: {bc_client}")
    # print(f"bc_client_other: {bc_client_other}")
    # print(f"ipfs_client: {ipfs_client}")
    # print(f"ipfs_client_other: {ipfs_client_other}")

    # print(connect_bc("cloud_two"))
    # print(connect_ipfs())
    # bc_client = connect_bc()
    # bc_client.unsubscribe('test')
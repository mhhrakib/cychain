from connect import connect_bc, connect_ipfs
from config import YEAR, date_format
from logger import Parser
from datetime import datetime, timedelta
import random, time


string1 = "cloud_one"
string2 = "cloud_two"

bc_client = connect_bc(string1)
print(bc_client)
ipfs_client = connect_ipfs(string1)
parser = Parser()
print(ipfs_client.id())
addresses = bc_client.getaddresses()
print(addresses)
num_addr = len(addresses)

if num_addr < 5:
    for _ in range(5-num_addr):
        bc_client.getnewaddress()


def get_timestamp(date_string):
    date_obj = datetime.strptime(
        YEAR + date_string, '%Y %b %d %H:%M:%S')  # 2021 Feb  1 09:25:31
    return (int)(datetime.timestamp(date_obj)), date_obj.strftime('%m-%d-%Y')


def create_metadata(plain_log):
    # ipfs_client = connect_ipfs(MINE)
    parser = Parser()
    parsed_logs = [parser.parse(line) for line in plain_log.splitlines()]
    start, dstart = get_timestamp(parsed_logs[0]["timestamp"])
    end, dend = get_timestamp(parsed_logs[-1]["timestamp"])
    # print(start, end, len(parsed_logs))
    ipfs_cid = ipfs_client.add_str(plain_log)
    bc_obj = {
        "fileID": "syslog",
        "id": ipfs_cid,
        "size": len(plain_log),
        "start": start,
        "end": end,
        "storage": "ipfs",
        "encrypted": "no",
        "num_records": len(parsed_logs),
        "publish_time": end + random.randint(1, 50)
    }
    
    return bc_obj, dstart, dend


def generate_keys(start, end):
    dates = [start]
    start_obj = datetime.strptime(start, date_format)
    end_obj = datetime.strptime(end, date_format)
    one_day = timedelta(days=1)
    start_obj += one_day

    while start_obj <= end_obj:
        print(start_obj)
        dates.append(start_obj.strftime(date_format))
        start_obj += one_day

    return dates


def publish_log(stream, from_addr, plain_log):
    if plain_log != "":
        metadata, start, end = create_metadata(plain_log)
        if start == end:
            txid = bc_client.publishfrom(from_addr, stream, start, {"json": metadata})
        else:
            txid = bc_client.publishfrom(from_addr, stream, generate_keys(
                start, end), {"json": metadata})

    return txid


def merge_lines(lines_array):
    res = ""
    for line in lines_array:
        res += line
    
    return res


addresses = bc_client.getaddresses()
print(addresses)
streams = bc_client.liststreams()
stream_names = [stream["name"] for stream in streams]
num_streams = len(stream_names)
num_addr = len(addresses)

for stream in stream_names:
    bc_client.subscribe(stream)
    
print(stream_names)

# print(bc_client.liststreams())

with open("output.txt", "r") as log_file:
    lines = log_file.readlines()

line_start = line_end = 100029
while line_end < 100000:
    line_end = line_start + random.randint(80, 150)
    pub_logs = merge_lines(lines[line_start: line_end])
    pub_stream = random.choice(stream_names)
    pub_addr = random.choice(addresses)
    txid = publish_log(pub_stream, pub_addr, pub_logs)
    print(f"Published log from: {line_start}--{line_end} in {pub_stream} from {pub_addr}")
    line_start = line_end
    time.sleep(1)

# for x, line in enumerate(lines[0:1500]):
#     print(x, parser.parse(line))


# print([parser.parse(line) for line in lines[0:150]])
# print(merge_lines(lines[0:15]))


# print(create_metadata(res))


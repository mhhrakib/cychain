from config import RPC_CONFIG_local, SYSLOG_PATH, MINE, YEAR, date_format
import mcrpc
from publish.logreader import follow
from logger import Parser
import json
from datetime import datetime, timedelta
from connect import connect_bc, connect_ipfs

string = "cloud_two"
string = ""
bc_client = connect_bc(string)
ipfs_client = connect_ipfs(string)


def get_timestamp(date_string):
    date_obj = datetime.strptime(
        YEAR + date_string, '%Y %b %d %H:%M:%S')  # 2021 Feb  1 09:25:31
    return (int)(datetime.timestamp(date_obj)), date_obj.strftime('%m-%d-%Y')


def create_metadata(plain_log):
    # ipfs_client = connect_ipfs(MINE)
    parser = Parser()
    parsed_logs = [parser.parse(line) for line in plain_log.splitlines()]
    start, dstart = get_timestamp(parsed_logs[0]["timestamp"])
    end, dend = get_timestamp(parsed_logs[-1]["timestamp"])
    # print(start, end, len(parsed_logs))
    ipfs_cid = ipfs_client.add_str(plain_log)
    bc_obj = {
        "fileID": "syslog",
        "id": ipfs_cid,
        "size": len(plain_log),
        "start": start,
        "end": end,
        "storage": "ipfs",
        "encrypted": "no"
    }
    
    return bc_obj, dstart, dend


def generate_keys(start, end):
    dates = [start]
    start_obj = datetime.strptime(start, date_format)
    end_obj = datetime.strptime(end, date_format)
    one_day = timedelta(days=1)
    start_obj += one_day

    while start_obj <= end_obj:
        print(start_obj)
        dates.append(start_obj.strftime(date_format))
        start_obj += one_day

    return dates


def publish_log(stream):
    # bc_client = connect_bc(MINE)
    for plain_log in follow(SYSLOG_PATH):
        if plain_log != "":
            metadata, start, end = create_metadata(plain_log, ipfs_client)
            if start == end:
                txid = bc_client.publish(stream, start, {"json": metadata})
            else:
                txid = bc_client.publish(stream, generate_keys(
                    start, end), {"json": metadata})



publish_log("ipfs")
# for plain_log in follow():
#     if plain_log != "":
#         metadata, start, end = create_metadata(plain_log)
#         if start == end:
#             bc_client.publish("ipfs", start, {"json": metadata})
#         else:
#             txid = bc_client.publish("ipfs", generate_keys(start, end), {"json": metadata})
# print(generate_keys('02-18-2021', '02-19-2021'))

import os
import time


def follow(name="syslog_test.txt", period=10):
    current = open(name, "r")
    curino = os.fstat(current.fileno()).st_ino
    while True:
        big_line = ""
        while True:
            line = current.readline()
            if not line:
                break
            # yield line
            big_line += line

        try:
            if os.stat(name).st_ino != curino:
                new = open(name, "r")
                current.close()
                current = new
                curino = os.fstat(current.fileno()).st_ino
                continue
        except IOError:
            pass
        # if big_line != "":
        #     yield big_line
        yield big_line
        time.sleep(period)


if __name__ == '__main__':
    fname = "/var/log/syslog"
    test = "syslog_test.txt"
    for l in follow(fname):
        print("LINE: {}".format(l))


# def follow(name):
#     current = open(name, "r")
#     curino = os.fstat(current.fileno()).st_ino
#     while True:
#         while True:
#             line = current.readline()
#             if not line:
#                 break
#             yield line

#         try:
#             if os.stat(name).st_ino != curino:
#                 new = open(name, "r")
#                 current.close()
#                 current = new
#                 curino = os.fstat(current.fileno()).st_ino
#                 continue
#         except IOError:
#             pass
#         time.sleep(10)

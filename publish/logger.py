import sys
from pyparsing import Word, alphas, Suppress, Combine, nums, string, Optional, Regex
from time import strftime
from datetime import datetime, timedelta
from config import YEAR
import random


class Parser(object):
    def __init__(self):
        ints = Word(nums)

        # priority
        priority = Suppress("<") + ints + Suppress(">")

        # timestamp
        month = Word(string.ascii_uppercase, string.ascii_lowercase, exact=3)
        day = ints
        hour = Combine(ints + ":" + ints + ":" + ints)

        timestamp = month + day + hour

        # hostname
        hostname = Word(alphas + nums + "_" + "-" + ".")

        # appname
        appname = Optional(Word(alphas + "/" + "-" + "_" + "." + nums)) + \
                  Optional(Suppress("[") + ints + Suppress("]")) + Optional(Suppress("]")) \
                  + Suppress(":")

        # message
        message = Regex(".*")

        # pattern build
        self.__pattern = timestamp + hostname + appname + message

    def parse(self, line):
        parsed = self.__pattern.parseString(line)
        # print(len(parsed))
        # print(type(parsed[0]), type(parsed[1]), type(parsed[2]))
        payload = {}
        # payload["priority"]  = parsed[0]
        # payload["timestamp"] = strftime("%Y-%m-%d %H:%M:%S")
        # print(len(parsed), parsed)
        timestamp = parsed[0] + " " + parsed[1] + " " + parsed[2]
        payload["timestamp"] = timestamp
        payload["hostname"] = parsed[3]
        payload["appname"] = parsed[4]
        if len(parsed) == 6:
            payload["message"] = parsed[5]
            payload["pid"] = 0
        else:
            payload["pid"] = parsed[5]
            payload["message"] = parsed[6]

        return payload


""" --------------------------------- """


def randomtimes(stime, etime, n):
    frmt = '%b %d %H:%M:%S'
    stime = datetime.strptime(stime, frmt)
    etime = datetime.strptime(etime, frmt)
    td = etime - stime
    rtime = [random.random() * td + stime for _ in range(n)]
    return sorted(rtime)


def main():
    #   print(strftime("%Y-%m-%d %H:%M:%S"))
    parser = Parser()

    # if len(sys.argv) == 1:
    #     print("Usage:\n  $ python xlog.py ./sample.log")
    #     exit(666)

    syslogPath = "input.txt"

    with open(syslogPath) as syslogFile:
        # list_of_months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
        months = [x for x in range(1, 11)]
        number_of_days = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
        line_count = 0
        month_index = 0
        day_index = 0
        logs_per_day = 0
        day = 1
        random_times = randomtimes("Jan 01 00:00:00", "Oct 31 23:59:59", 118000)
        for line in syslogFile:
            line_count += 1
            fields = parser.parse(line)
            # print(fields["timestamp"])
            # date_obj = datetime.strptime(YEAR + fields["timestamp"], '%Y %b %e %H:%M:%S')  # 2021 Feb  1 09:25:31
            date_obj = random_times[line_count]
            # fields["timestamp"] = fields["timestamp"].replace(fields["timestamp"][0:6], list_of_months[month_index] + " " + str(day), 1)
            fields["timestamp"] = date_obj.strftime('%b %e %H:%M:%S')
            # if len(fields) == 5 :
            #     new_line = fields["timestamp"] + " " + fields["hostname"] + " " + fields["appname"] + " " + fields["pid"] + " " + fields["message"]
            # else:
            #     new_line = fields["timestamp"] + " " + fields["hostname"] + " " + fields["appname"] + " " + fields["message"]

            new_line = fields["timestamp"] + line[len(fields["timestamp"]):]

            f = open("output.txt", "a")
            f.write(new_line)
            f.close()
            logs_per_day += 1
            if logs_per_day >= 400:
                day = day + 1
                if day >= number_of_days[month_index]:
                    day = 1
                    month_index += 1
                    if month_index >= 12:
                        break
                logs_per_day = 0


if __name__ == "__main__":
    main()
    # ls = randomtimes("Jan 01 00:00:00", "Oct 31 23:59:59", 118000)
    # print(ls)
    # for date in ls:
    #     print(date.strftime('%b %-d %H:%M:%S'))
from flask import Flask
from flask import render_template
from api.stream import stream_bp
from api.retrieve import retrieve_bp
from api.home import home_bp
from api.ipfs import ipfs_bp
from api.blockchain import bc_bp

app = Flask(__name__)
app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'
app.register_blueprint(stream_bp)
app.register_blueprint(retrieve_bp)
app.register_blueprint(home_bp)
app.register_blueprint(bc_bp)
app.register_blueprint(ipfs_bp)


@app.route('/')
def hello_world():  # put application's code here
    return render_template('home.html')


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)


